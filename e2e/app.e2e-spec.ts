import { ExaminationPage } from './app.po';

describe('examination App', () => {
  let page: ExaminationPage;

  beforeEach(() => {
    page = new ExaminationPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
