import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {


  constructor( private service:ApiService){}

  ngOnInit(){


  }

  remove(data){
  this.service.userList.splice(data,1);
}

}
