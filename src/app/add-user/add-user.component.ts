import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {


name : string = "";
email : string = "";
address : string = "";
company : string = "";



  constructor( private service:ApiService){}

  ngOnInit(){

  }

  addUser(){
  this.service.userList.push( { "name":this.name,"email":this.email,"address":this.address,"company":this.company, });

}


}
