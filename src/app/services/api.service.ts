import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ApiService {

  constructor( private http:Http) {
         this.http.get('https://jsonplaceholder.typicode.com/users')
                    .map(result => result.json())                     // untuk mengubah fil JSON menjadi hanya variable di dalam app.component.ts
                    .catch(error => Observable.throw(error.json().error) || "Server Error")
                     .subscribe(result => this.userList = result );   //
   }

    userList:object[];



addData(obj : object){
  let body = JSON.stringify(obj);
  let headers = new Headers({ "Content-Type" : "application/json" });
  let options = new RequestOptions({ headers : headers });

  this.http.post('https://jsonplaceholder.typicode.com/users',body, options)
  .subscribe(result => console.log(result.json()));
}



}
